import React from 'react'

import '../popup.styles.css'

export default props => props.show ? (
    <div>
        <div className='popup'>
            <form onSubmit={e => {
                e.preventDefault()
                props.onSubmit(props.id)
            }}>
                <label for='name'>Project Name</label>
                <input type='text'
                    id='name'
                    placeholder='Please enter the name of your project...'
                    value={props.projectInputName}
                    onChange={props.registerProjectInputNameChange}
                    required />
                <label for='cust'>Customer</label>
                <input type='text'
                    id='cust'
                    placeholder='Please specify the customer that hired you for this project...'
                    value={props.projectInputCustomer}
                    onChange={props.registerProjectInputCustomerChange}
                    required />
                <label for='rate'>Hourly Rate</label>
                <input type='number'
                    id='rate'
                    min={100} max={999}
                    placeholder='Please enter your hourly rate for this project in DKK...'
                    value={props.projectInputHourlyRate}
                    onChange={props.registerProjectInputHourlyRateChange}
                    required />
                <button className='btn .btn-default' type='submit'>Create</button>
            </form>

        </div>
        <div className='backdrop' onClick={props.onCancel} />
    </div>
) : null

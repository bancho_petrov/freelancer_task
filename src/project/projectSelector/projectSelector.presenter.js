import React from 'react'

import './projectSelector.styles.css'

export default ({projects, selected, selectProject, promptNewProject}) => (
    <div className='selector-container'>
        <p>Please choose one of your projects or create a new one:</p>
        <div className='dropdown'>
            <button className='btn btn-default dropdown-toggle' data-toggle='dropdown'>
                {selected ? `${projects[selected].name} ` : 'My Projects '}
                <span className='caret'></span>
            </button>
            <ul className='dropdown-menu'>
                {(Object.keys(projects).length > 0)
                    ? Object.keys(projects).map(id => (
                        <li key={id} onClick={() => selectProject(id)}>{projects[id].name}</li>
                    ))
                    : 'No Projects Available'}
                <li className='divider'></li>
                <li onClick={promptNewProject}>Create new project</li>
            </ul>
        </div>
    </div>
)

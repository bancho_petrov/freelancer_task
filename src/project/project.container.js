import React, {Component} from 'react'
import ProjectSelector from './projectSelector/projectSelector.presenter'
import ProjectView from './projectView/projectView.presenter'
import CreateProjectPopup from './createProjectPopup/createProjectPopup.presenter'
import AddSessionPopup from './addSessionPopup/addSessionPopup.presenter'

import uuidv1 from 'uuid/v1'
import moment from 'moment'

import './project.styles.css'

export default class extends Component {

    constructor(props){
        super(props)
        this.state = {
            projectsById: { //dummy project
                some_id: {
                    name: 'Project X',
                    customer: 'Reviso',
                    hourlyRate: 180, // in dkk
                    sessionsWorked: [ //timeSpent is in minutes
                        {
                            date: moment('2017-07-24'),
                            timeSpent: 180,
                            comments: 'created front-end components'
                        },
                        {
                            date: moment('2017-07-25'),
                            timeSpent: 98,
                            comments: 'implemented back-end'
                        },
                        {
                            date: moment('2017-07-26'),
                            timeSpent: 150,
                            comments: 'integrated with third-party service'
                        }
                    ]
                }
            },
            selectedProjectId: null,
            projectPopupVisible: false,
            sessionPopupVisible: false,

            // create project input fields 
            projectInputName: '',
            projectInputCustomer: '',
            projectInputHourlyRate: '',

            // add session input fields
            sessionInputDate: '',
            sessionInputHoursSpent: '',
            sessionInputMinutesSpent: '',
            sessionInputComments: ''
        }
    }



    toggleProjectPopup = () => this.setState(prevState => ({projectPopupVisible: !prevState.projectPopupVisible}))

    registerProjectInputNameChange = event => this.setState({projectInputName: event.target.value})
    registerProjectInputCustomerChange = event => this.setState({projectInputCustomer: event.target.value})
    registerProjectInputHourlyRateChange = event => this.setState({projectInputHourlyRate: parseInt(event.target.value, 10)})

    submitProjectForm = () => {
        this.addNewProject({
            name: this.state.projectInputName,
            customer: this.state.projectInputCustomer,
            hourlyRate: this.state.projectInputHourlyRate
        })
        this.resetProjectForm()
    }

    resetProjectForm = () => this.setState({
        projectInputName: '',
        projectInputCustomer: '',
        projectInputHourlyRate: ''
    })

    addNewProject = projectData => {
        const newId = uuidv1() //v1 should suffice for this example :)
        this.setState(prevState => ({
            projectsById: {
                ...prevState.projectsById,
                [newId]: {
                    ...projectData,
                    sessionsWorked: []
                }
            },
            selectedProjectId: newId
        }))
        this.toggleProjectPopup()
        console.log('created project', projectData, 'with id', newId)
    }



    toggleSessionPopup = () => this.setState(prevState => ({sessionPopupVisible: !prevState.sessionPopupVisible}))

    registerSessionInputDateChange = event => this.setState({sessionInputDate: event.target.value})
    registerSessionInputHoursSpentChange = event => this.setState({sessionInputHoursSpent: parseInt(event.target.value, 10)})
    registerSessionInputMinutesSpentChange = event => this.setState({sessionInputMinutesSpent: parseInt(event.target.value, 10)})
    registerSessionInputCommentsChange = event => this.setState({sessionInputComments: event.target.value})

    submitSessionForm = projectId => {
        this.addSessionToProject({
            date: this.state.sessionInputDate,
            timeSpent: (this.state.sessionInputHoursSpent * 60) + this.state.sessionInputMinutesSpent,
            comments: this.state.sessionInputComments
        }, projectId)
        this.resetSessionForm()
    }

    resetSessionForm = () => this.setState({
        sessionInputDate: '',
        sessionInputHoursSpent: '',
        sessionInputMinutesSpent: '',
        sessionInputComments: ''
    })

    addSessionToProject = (sessionData, projectId) => {
        // console.log(session.date)
        sessionData.date = moment(sessionData.date)
        this.setState(prevState => ({
            projectsById: {
                ...prevState.projectsById,
                [projectId]: {
                    ...prevState.projectsById[projectId],
                    sessionsWorked: prevState.projectsById[projectId].sessionsWorked.concat(sessionData)
                }
            }
        }))
        this.toggleSessionPopup()
        console.log('added session', sessionData, 'to project with id', projectId)
    }
    


    selectProject = id => this.setState({selectedProjectId: id}, () => console.log('selected project', this.state.projectsById[id]))
    getSelectedProject = () => this.state.selectedProjectId && this.state.projectsById[this.state.selectedProjectId]


    render = () => (
        <div className='project'>
            <ProjectSelector
                projects={this.state.projectsById}
                selected={this.state.selectedProjectId}
                selectProject={this.selectProject}
                promptNewProject={this.toggleProjectPopup}
            />
            <ProjectView
                id={this.state.selectedProjectId}
                promptNewSession={this.toggleSessionPopup}
                selectedProject={this.getSelectedProject()}
            />
            <CreateProjectPopup
                show={this.state.projectPopupVisible}
                onCancel={this.toggleProjectPopup}
                onSubmit={this.submitProjectForm}
                projectInputName={this.state.projectInputName}
                projectInputCustomer={this.state.projectInputCustomer}
                projectInputHourlyRate={this.state.projectInputHourlyRate}
                registerProjectInputNameChange={this.registerProjectInputNameChange}
                registerProjectInputCustomerChange={this.registerProjectInputCustomerChange}
                registerProjectInputHourlyRateChange={this.registerProjectInputHourlyRateChange}
            />
            <AddSessionPopup
                show={this.state.sessionPopupVisible}
                id={this.state.selectedProjectId}
                onCancel={this.toggleSessionPopup}
                onSubmit={this.submitSessionForm}
                sessionInputDate={this.state.sessionInputDate}
                sessionInputHoursSpent={this.state.sessionInputHoursSpent}
                sessionInputMinutesSpent={this.state.sessionInputMinutesSpent}
                sessionInputComments={this.state.sessionInputComments}
                registerSessionInputDateChange={this.registerSessionInputDateChange}
                registerSessionInputHoursSpentChange={this.registerSessionInputHoursSpentChange}
                registerSessionInputMinutesSpentChange={this.registerSessionInputMinutesSpentChange}
                registerSessionInputCommentsChange={this.registerSessionInputCommentsChange}
            />
        </div>
    )
}

import React from 'react'

import '../popup.styles.css'

export default props => props.show ? (
    <div>
        <div className='popup'>
            <form onSubmit={e => {
                e.preventDefault()
                props.onSubmit(props.id)
            }} >
                <label for='date'>Date</label>
                <input type='date'
                    id='date'
                    value={props.sessionInputDate}
                    onChange={props.registerSessionInputDateChange}
                    required />

                <label for='timeSpent'>Time Spent</label>
                <div className='timeInputs'>
                    <input type='number'
                        min={0}
                        className='timeSpent'
                        placeholder='h'
                        value={props.sessionInputHoursSpent}
                        onChange={props.registerSessionInputHoursSpentChange}
                        required /> 
                        
                        :

                    <input type='number'
                        min={0} max={59}
                        className='timeSpent'
                        placeholder='m'
                        value={props.sessionInputMinutesSpent}
                        onChange={props.registerSessionInputMinutesSpentChange}
                        required />
                </div>
                    
                <label for='comments'>Comments</label>
                <input type='text'
                    id='comments'
                    placeholder='Please write any comments here...'
                    value={props.sessionInputComments}
                    onChange={props.registerSessionInputCommentsChange}
                     />
                <button type='submit' className='btn .btn-default'>Create</button>
            </form>

        </div>
        <div className='backdrop' onClick={props.onCancel} />
    </div>
) : null

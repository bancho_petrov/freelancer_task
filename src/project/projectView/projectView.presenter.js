import React from 'react'

import Table from './table/table.presenter'

import './projectView.styles.css'

export default ({id, promptNewSession, selectedProject}) => {

    const {
        name,
        customer,
        hourlyRate,
        sessionsWorked
    }  = selectedProject || {}

    const totalTimeSpentInM = id && sessionsWorked.reduce((acc, ses) => acc + ses.timeSpent, 0)
    const totalMoneyEarned = totalTimeSpentInM && ((totalTimeSpentInM / 60) * hourlyRate)

    const prependZero = n => (n < 10) ? `0${n}` : n

    const minsToHoursStr = mins => `${prependZero(Math.floor(mins / 60))}:${prependZero(mins % 60)} h`

    return id ? (
        <div className="view-container">
            <div className='heading'>
                <div className='heading-item'>
                    <h4>Customer:</h4> <h3>{customer}</h3>
                </div>
                <div className='heading-item'>
                    <h4>Hourly Rate:</h4> <h3>{hourlyRate} DKK</h3>
                </div>
            </div>
            {(id && sessionsWorked.length > 0) ? (
                <Table 
                    name={name}
                    sessionsWorked={sessionsWorked}
                    minsToHoursStr={minsToHoursStr}
                />
            ) : (<h2>No sessions worked for this project yet</h2>)}
            <button className='btn btn-primary' onClick={promptNewSession}>Add Session</button>
            <div className='summary'>
                <div className='summary-item'>
                    <h4>Total Time Spent:</h4> <h3>{minsToHoursStr(totalTimeSpentInM)}</h3>
                </div>
                <div className='summary-item'>
                    <h4>Total Money Earned:</h4> <h3>{totalMoneyEarned.toLocaleString('en', {useGrouping:true, maximumFractionDigits: 2})} DKK</h3>
                </div>
            </div>
        </div>
    ) : (
        <div className="view-container">
            <h1 className='empty'>Select a project to manage it here</h1>
        </div>
    )

}

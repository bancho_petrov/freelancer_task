import React from 'react'
import moment from 'moment'

import './table.styles.css'

export default ({name, sessionsWorked, minsToHoursStr}) => (
    <div>
        <h2 className='intro'>Sessions Worked on {name}</h2>
        <table className='table'>
            <thead>
            <tr>
                <th>Date</th>
                <th>Time Spent</th>
                <th>Comments</th>
            </tr>
            </thead>
            <tbody>
                {sessionsWorked.sort((a, b) => moment(a.date).valueOf() - moment(b.date).valueOf())
                    .map(({date, timeSpent, comments}, index) => (
                    <tr key={index}>
                        <td>{date.format('dddd, MMMM Do YYYY')}</td>
                        <td>{minsToHoursStr(timeSpent)}</td>
                        <td>{comments}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </div>
)

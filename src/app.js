import React, {Component} from 'react'
import './app.css'

import ProjectContainer from './project/project.container'

export default class App extends Component {
  render() {
    return (
      <div className='app'>
        <div className='app-header'>
          <h2>Welcome, Mr Freelancer!</h2>
        </div>
        <ProjectContainer />
      </div>
    )
  }
}
